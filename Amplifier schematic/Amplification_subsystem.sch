EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Voltage Switching And Amplifier Subsystem"
Date ""
Rev "01"
Comp "Golden Key"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 2850 3500 0    50   Input ~ 0
Vselected
$Comp
L Device:R R1
U 1 1 60BA0C0B
P 4700 3850
F 0 "R1" H 4770 3896 50  0000 L CNN
F 1 "5000" H 4770 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4630 3850 50  0001 C CNN
F 3 "~" H 4700 3850 50  0001 C CNN
	1    4700 3850
	1    0    0    -1  
$EndComp
$Comp
L Diode:BZX84Cxx D1
U 1 1 60BA1751
P 4700 4600
F 0 "D1" V 4654 4680 50  0000 L CNN
F 1 "BZX84Cxx" V 4745 4680 50  0000 L CNN
F 2 "Diode_SMD:D_0201_0603Metric" H 4700 4425 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzx84c2v4.pdf" H 4700 4600 50  0001 C CNN
	1    4700 4600
	0    1    1    0   
$EndComp
Text GLabel 4700 4750 3    50   Input ~ 0
GND
Wire Wire Line
	4700 4450 4700 4050
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 60BA2B9E
P 6300 3700
F 0 "Q1" V 6628 3700 50  0000 C CNN
F 1 "2N2219" V 6537 3700 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 6500 3625 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 6300 3700 50  0001 L CNN
	1    6300 3700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4700 3700 4700 3600
Connection ~ 4700 3600
Connection ~ 4700 4050
Wire Wire Line
	4700 4050 4700 4000
$Comp
L Device:R R2
U 1 1 60BA4204
P 7000 4250
F 0 "R2" H 7070 4296 50  0000 L CNN
F 1 "4677" H 7070 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6930 4250 50  0001 C CNN
F 3 "~" H 7000 4250 50  0001 C CNN
	1    7000 4250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60BA4894
P 7000 4950
F 0 "R3" H 7070 4996 50  0000 L CNN
F 1 "10000" H 7070 4905 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6930 4950 50  0001 C CNN
F 3 "~" H 7000 4950 50  0001 C CNN
	1    7000 4950
	1    0    0    -1  
$EndComp
Text GLabel 7000 5100 3    50   Input ~ 0
GND
Wire Wire Line
	7000 4400 7000 4650
$Comp
L Device:R R4
U 1 1 60BA60AD
P 7400 3600
F 0 "R4" V 7193 3600 50  0000 C CNN
F 1 "4100" V 7284 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 7330 3600 50  0001 C CNN
F 3 "~" H 7400 3600 50  0001 C CNN
	1    7400 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 3600 7000 3600
Wire Wire Line
	7000 4100 7000 3600
Connection ~ 7000 3600
Wire Wire Line
	7000 3600 7250 3600
Connection ~ 7000 4650
Wire Wire Line
	7000 4650 7000 4800
Text HLabel 3450 3300 1    50   Input ~ 0
12Vcc
Text GLabel 7550 3600 2    50   Output ~ 0
Voutpi
$Comp
L LM741CN_NOPB:LM741CN_NOPB IC3
U 1 1 60C6F440
P 3750 2900
F 0 "IC3" H 4900 3165 50  0000 C CNN
F 1 "LM741CN_NOPB" H 4900 3074 50  0000 C CNN
F 2 "LM741CN_NOPB:DIP794W53P254L959H508Q8N" H 5900 3000 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/1230/0900766b81230d6c.pdf" H 5900 2900 50  0001 L CNN
F 4 "LM741 Operational Amplifier 1MHz DIP8 Texas Instruments LM741CN/NOPB Op Amp, 1MHz, 8-Pin MDIP" H 5900 2800 50  0001 L CNN "Description"
F 5 "5.08" H 5900 2700 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5900 2600 50  0001 L CNN "Manufacturer_Name"
F 7 "LM741CN/NOPB" H 5900 2500 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "926-LM741CN/NOPB" H 5900 2400 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/LM741CN-NOPB?qs=QbsRYf82W3Gt6%252BDX6%252BuAjw%3D%3D" H 5900 2300 50  0001 L CNN "Mouser Price/Stock"
F 10 "LM741CN/NOPB" H 5900 2200 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/lm741cnnopb/texas-instruments?region=nac" H 5900 2100 50  0001 L CNN "Arrow Price/Stock"
	1    3750 2900
	1    0    0    -1  
$EndComp
$Comp
L LM741CN_NOPB:LM741CN_NOPB IC4
U 1 1 60C6FB8A
P 3950 5550
F 0 "IC4" H 5100 5815 50  0000 C CNN
F 1 "LM741CN_NOPB" H 5100 5724 50  0000 C CNN
F 2 "LM741CN_NOPB:DIP794W53P254L959H508Q8N" H 6100 5650 50  0001 L CNN
F 3 "http://docs-emea.rs-online.com/webdocs/1230/0900766b81230d6c.pdf" H 6100 5550 50  0001 L CNN
F 4 "LM741 Operational Amplifier 1MHz DIP8 Texas Instruments LM741CN/NOPB Op Amp, 1MHz, 8-Pin MDIP" H 6100 5450 50  0001 L CNN "Description"
F 5 "5.08" H 6100 5350 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 6100 5250 50  0001 L CNN "Manufacturer_Name"
F 7 "LM741CN/NOPB" H 6100 5150 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "926-LM741CN/NOPB" H 6100 5050 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/LM741CN-NOPB?qs=QbsRYf82W3Gt6%252BDX6%252BuAjw%3D%3D" H 6100 4950 50  0001 L CNN "Mouser Price/Stock"
F 10 "LM741CN/NOPB" H 6100 4850 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/lm741cnnopb/texas-instruments?region=nac" H 6100 4750 50  0001 L CNN "Arrow Price/Stock"
	1    3950 5550
	1    0    0    -1  
$EndComp
Text GLabel 3750 3200 0    50   Input ~ 0
GND
Text GLabel 3750 3000 0    50   Input ~ 0
GND
Wire Wire Line
	3100 3500 3100 3100
Wire Wire Line
	3100 3100 3750 3100
Wire Wire Line
	3100 3500 2850 3500
Wire Wire Line
	6050 3100 6100 3100
Wire Wire Line
	6100 3100 6100 3450
Wire Wire Line
	6100 3450 4550 3450
Wire Wire Line
	4550 3450 4550 3600
Wire Wire Line
	4550 3600 4700 3600
Text HLabel 6050 3000 1    50   Input ~ 0
12Vcc
NoConn ~ 3750 2900
NoConn ~ 6050 3200
NoConn ~ 6050 2900
Text GLabel 3950 5850 3    50   Input ~ 0
GND
Wire Wire Line
	6300 5750 6250 5750
Wire Wire Line
	7000 4650 7350 4650
Wire Wire Line
	7350 4650 7350 6150
Wire Wire Line
	7350 6150 3750 6150
Wire Wire Line
	3750 6150 3750 5650
Wire Wire Line
	3750 5650 3950 5650
Wire Wire Line
	4700 4050 3900 4050
Wire Wire Line
	3900 4050 3900 5750
Wire Wire Line
	3900 5750 3950 5750
Wire Wire Line
	6250 5650 6450 5650
Wire Wire Line
	6450 5650 6450 3950
Wire Wire Line
	6450 3950 5650 3950
Wire Wire Line
	5650 3950 5650 3600
Connection ~ 5650 3600
Wire Wire Line
	5650 3600 6100 3600
Wire Wire Line
	6300 3900 6300 5750
Wire Wire Line
	4700 3600 5650 3600
NoConn ~ 3950 5550
NoConn ~ 6250 5550
NoConn ~ 6250 5850
$EndSCHEMATC
