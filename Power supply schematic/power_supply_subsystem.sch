EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Power Supply Sub-System"
Date ""
Rev "01"
Comp "Golden Key"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:FDN340P Q2
U 1 1 60BA9E14
P 9150 2750
F 0 "Q2" H 9354 2796 50  0000 L CNN
F 1 "FDN340P" H 9354 2705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9350 2675 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/FDN340P-D.PDF" H 9150 2750 50  0001 L CNN
	1    9150 2750
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 60BAAD5E
P 8350 3100
F 0 "R9" H 8420 3146 50  0000 L CNN
F 1 "10000" H 8420 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 8280 3100 50  0001 C CNN
F 3 "~" H 8350 3100 50  0001 C CNN
	1    8350 3100
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N5820 D4
U 1 1 60BAB9A0
P 8850 3100
F 0 "D4" H 8850 2883 50  0000 C CNN
F 1 "1N5820" H 8850 2974 50  0000 C CNN
F 2 "Diode_SMD:D_0201_0603Metric" H 8850 2925 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88526/1n5820.pdf" H 8850 3100 50  0001 C CNN
	1    8850 3100
	-1   0    0    1   
$EndComp
Text Label 8100 2750 2    50   ~ 0
5VBuck
Wire Wire Line
	8950 2750 8700 2750
Wire Wire Line
	8350 2950 8350 2750
Connection ~ 8350 2750
Wire Wire Line
	8350 2750 8100 2750
Wire Wire Line
	8700 3100 8700 2750
Connection ~ 8700 2750
Wire Wire Line
	8700 2750 8350 2750
Text GLabel 8350 3250 3    50   Output ~ 0
GND
$Comp
L Transistor_FET:FDN340P Q3
U 1 1 60BAC4D5
P 9250 4100
F 0 "Q3" V 9499 4100 50  0000 C CNN
F 1 "FDN340P" V 9590 4100 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9450 4025 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/FDN340P-D.PDF" H 9250 4100 50  0001 L CNN
	1    9250 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	9250 2950 9250 3100
Wire Wire Line
	9000 3100 9250 3100
Connection ~ 9250 3100
Wire Wire Line
	9250 3100 9250 3650
$Comp
L Diode:1N5820 D3
U 1 1 60BAE68C
P 8500 3850
F 0 "D3" V 8546 3770 50  0000 R CNN
F 1 "1N5820" V 8455 3770 50  0000 R CNN
F 2 "Diode_SMD:D_0201_0603Metric" H 8500 3675 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88526/1n5820.pdf" H 8500 3850 50  0001 C CNN
	1    8500 3850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9250 3650 8500 3650
Wire Wire Line
	8500 3650 8500 3700
Connection ~ 9250 3650
Wire Wire Line
	9250 3650 9250 3800
Text HLabel 8050 4200 0    50   Output ~ 0
Vselected
Wire Wire Line
	9050 4200 8500 4200
Wire Wire Line
	8500 4000 8500 4200
Connection ~ 8500 4200
Wire Wire Line
	8500 4200 8050 4200
Text HLabel 10000 4200 2    50   Input ~ 0
Battery+
$Comp
L Device:LED D5
U 1 1 60BAF882
P 9400 3800
F 0 "D5" H 9393 3545 50  0000 C CNN
F 1 "LED" H 9393 3636 50  0000 C CNN
F 2 "LED_SMD:LED_0201_0603Metric" H 9400 3800 50  0001 C CNN
F 3 "~" H 9400 3800 50  0001 C CNN
	1    9400 3800
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D6
U 1 1 60BB052E
P 9700 3950
F 0 "D6" V 9647 4030 50  0000 L CNN
F 1 "LED" V 9738 4030 50  0000 L CNN
F 2 "LED_SMD:LED_0201_0603Metric" H 9700 3950 50  0001 C CNN
F 3 "~" H 9700 3950 50  0001 C CNN
	1    9700 3950
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 60BB093F
P 9900 3800
F 0 "R10" V 9693 3800 50  0000 C CNN
F 1 "1000" V 9784 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 9830 3800 50  0001 C CNN
F 3 "~" H 9900 3800 50  0001 C CNN
	1    9900 3800
	0    1    1    0   
$EndComp
Text GLabel 10050 3800 2    50   Input ~ 0
GND
Wire Wire Line
	9450 4200 9700 4200
Wire Wire Line
	9700 4100 9700 4200
Connection ~ 9700 4200
Wire Wire Line
	9700 4200 10000 4200
Connection ~ 9250 3800
Wire Wire Line
	9250 3800 9250 3900
Wire Wire Line
	9550 3800 9700 3800
Connection ~ 9700 3800
Wire Wire Line
	9700 3800 9750 3800
$Comp
L Device:R R11
U 1 1 60BB350E
P 10700 4000
F 0 "R11" H 10630 3954 50  0000 R CNN
F 1 "10000" H 10630 4045 50  0000 R CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 10630 4000 50  0001 C CNN
F 3 "~" H 10700 4000 50  0001 C CNN
	1    10700 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	9250 3650 10700 3650
Wire Wire Line
	10700 3850 10700 3650
Text GLabel 10700 4150 3    50   Input ~ 0
GND
Text HLabel 10700 3650 2    50   Output ~ 0
Vchrg
Text GLabel 9250 2550 2    50   Input ~ 0
GND
$Comp
L MP1584EN-LF-P:MP1584EN-LF-P IC1
U 1 1 60BCC730
P 3600 3300
F 0 "IC1" H 4200 3565 50  0000 C CNN
F 1 "MP1584EN-LF-P" H 4200 3474 50  0000 C CNN
F 2 "MP1584EN-LF-P:SOIC127P600X170-9N" H 4650 3400 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/MP1584EN-LF-P.pdf" H 4650 3300 50  0001 L CNN
F 4 "Switching Voltage Regulators NRFND: USE MP2338 as replacement" H 4650 3200 50  0001 L CNN "Description"
F 5 "1.7" H 4650 3100 50  0001 L CNN "Height"
F 6 "Monolithic Power Systems (MPS)" H 4650 3000 50  0001 L CNN "Manufacturer_Name"
F 7 "MP1584EN-LF-P" H 4650 2900 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "946-MP1584EN-LF-P" H 4650 2800 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Monolithic-Power-Systems-MPS/MP1584EN-LF-P?qs=%252B6g0mu59x7Ie1GinT4nULQ%3D%3D" H 4650 2700 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 4650 2600 50  0001 L CNN "Arrow Part Number"
F 11 "" H 4650 2500 50  0001 L CNN "Arrow Price/Stock"
	1    3600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 2550 4550 2550
Wire Wire Line
	5200 2550 5200 3400
Wire Wire Line
	5200 3400 4800 3400
$Comp
L Device:R R5
U 1 1 60BD802D
P 3250 4200
F 0 "R5" H 3320 4246 50  0000 L CNN
F 1 "100000" H 3320 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 3180 4200 50  0001 C CNN
F 3 "~" H 3250 4200 50  0001 C CNN
	1    3250 4200
	1    0    0    -1  
$EndComp
$Comp
L pspice:CAP C2
U 1 1 60BD9427
P 3250 3800
F 0 "C2" H 3428 3846 50  0000 L CNN
F 1 "0.000000000150" H 3428 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 3250 3800 50  0001 C CNN
F 3 "~" H 3250 3800 50  0001 C CNN
	1    3250 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 3500 3250 3500
Wire Wire Line
	3250 3500 3250 3550
Wire Wire Line
	3250 4350 3250 4500
Wire Wire Line
	4800 3600 5200 3600
$Comp
L pspice:CAP C4
U 1 1 60BDCBB9
P 5850 3250
F 0 "C4" H 6028 3296 50  0000 L CNN
F 1 "10u" H 6028 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5850 3250 50  0001 C CNN
F 3 "~" H 5850 3250 50  0001 C CNN
	1    5850 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 3000 5850 2150
Wire Wire Line
	5850 2150 4550 2150
Wire Wire Line
	4550 2150 4550 2550
Connection ~ 4550 2550
Wire Wire Line
	4550 2550 5200 2550
Wire Wire Line
	4800 3500 5500 3500
Wire Wire Line
	5200 3600 5200 3850
$Comp
L Device:R R8
U 1 1 60BE09DB
P 5500 3650
F 0 "R8" H 5570 3696 50  0000 L CNN
F 1 "100k" H 5570 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 5430 3650 50  0001 C CNN
F 3 "~" H 5500 3650 50  0001 C CNN
	1    5500 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3800 5500 3850
Wire Wire Line
	5500 3850 5200 3850
Wire Wire Line
	5500 3850 5850 3850
Wire Wire Line
	5850 3500 5850 3850
Connection ~ 5500 3850
NoConn ~ 3600 3400
$Comp
L Device:R R7
U 1 1 60BE29AB
P 5000 4400
F 0 "R7" V 5207 4400 50  0000 C CNN
F 1 "221k" V 5116 4400 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4930 4400 50  0001 C CNN
F 3 "~" H 5000 4400 50  0001 C CNN
	1    5000 4400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3600 3600 3600 4400
Wire Wire Line
	3600 4400 4650 4400
$Comp
L Device:R R6
U 1 1 60BE3E87
P 4650 4700
F 0 "R6" H 4720 4746 50  0000 L CNN
F 1 "40.2" H 4720 4655 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4580 4700 50  0001 C CNN
F 3 "~" H 4650 4700 50  0001 C CNN
	1    4650 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4850 4650 4900
Wire Wire Line
	4650 4400 4650 4550
Connection ~ 4650 4400
Wire Wire Line
	4650 4400 4850 4400
Text Label 6350 5450 2    50   ~ 0
5VBuck
$Comp
L Device:L L1
U 1 1 60BE6CA1
P 5400 5450
F 0 "L1" V 5590 5450 50  0000 C CNN
F 1 "4.7u" V 5499 5450 50  0000 C CNN
F 2 "Inductor_SMD:L_0201_0603Metric" H 5400 5450 50  0001 C CNN
F 3 "~" H 5400 5450 50  0001 C CNN
	1    5400 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3600 3300 2900 3300
Wire Wire Line
	2900 3300 2900 4200
Wire Wire Line
	2900 5450 5200 5450
Wire Wire Line
	5550 5450 5700 5450
Wire Wire Line
	5150 4400 5950 4400
Wire Wire Line
	5950 4400 5950 5450
Connection ~ 5950 5450
Wire Wire Line
	5950 5450 6350 5450
$Comp
L pspice:CAP C1
U 1 1 60BEB459
P 2500 3700
F 0 "C1" H 2678 3746 50  0000 L CNN
F 1 "100n" H 2678 3655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 2500 3700 50  0001 C CNN
F 3 "~" H 2500 3700 50  0001 C CNN
	1    2500 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3300 4900 3300
Wire Wire Line
	4900 3300 4900 2850
Wire Wire Line
	4900 2850 2500 2850
Wire Wire Line
	2500 2850 2500 3450
Wire Wire Line
	2500 3950 2500 4200
Wire Wire Line
	2500 4200 2900 4200
Connection ~ 2900 4200
Wire Wire Line
	2900 4200 2900 5450
Connection ~ 5700 5450
Wire Wire Line
	5700 5450 5950 5450
$Comp
L pspice:CAP C3
U 1 1 60BF1B43
P 5700 5700
F 0 "C3" H 5878 5746 50  0000 L CNN
F 1 "22u" H 5878 5655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5700 5700 50  0001 C CNN
F 3 "~" H 5700 5700 50  0001 C CNN
	1    5700 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_ALT D2
U 1 1 60BF2C24
P 5200 5700
F 0 "D2" V 5154 5780 50  0000 L CNN
F 1 "D_Schottky_ALT" V 5245 5780 50  0000 L CNN
F 2 "Diode_SMD:D_0201_0603Metric" H 5200 5700 50  0001 C CNN
F 3 "~" H 5200 5700 50  0001 C CNN
	1    5200 5700
	0    1    1    0   
$EndComp
Wire Wire Line
	5200 5450 5200 5550
Connection ~ 5200 5450
Wire Wire Line
	5200 5450 5250 5450
Wire Wire Line
	5200 5850 5200 6050
Wire Wire Line
	5200 6050 5500 6050
Wire Wire Line
	5700 6050 5700 5950
Wire Wire Line
	5500 6050 5500 6250
Connection ~ 5500 6050
Wire Wire Line
	5500 6050 5700 6050
$Comp
L Connector:Barrel_Jack J2
U 1 1 60C07D17
P 2000 2500
F 0 "J2" H 2057 2825 50  0000 C CNN
F 1 "Barrel_Jack" H 2057 2734 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 2050 2460 50  0001 C CNN
F 3 "~" H 2050 2460 50  0001 C CNN
	1    2000 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 2600 2450 2600
Wire Wire Line
	2450 2750 2450 2600
Connection ~ 2450 2600
Wire Wire Line
	2450 2600 2700 2600
Wire Wire Line
	2300 2400 2600 2400
Wire Wire Line
	4250 2400 4250 2550
Wire Wire Line
	2700 2500 2600 2500
Wire Wire Line
	2600 2500 2600 2400
Connection ~ 2600 2400
Wire Wire Line
	2600 2400 3000 2400
Text GLabel 5500 6250 3    50   Input ~ 0
GND
Text GLabel 4650 4900 3    50   Input ~ 0
GND
Text GLabel 3250 4500 3    50   Input ~ 0
GND
Text GLabel 2450 2750 3    50   Input ~ 0
GND
Text GLabel 4200 4100 3    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 60C09943
P 2900 2500
F 0 "J3" H 2980 2492 50  0000 L CNN
F 1 "Conn_01x02" H 2980 2401 50  0000 L CNN
F 2 "Connector_PinHeader_1.00mm:PinHeader_1x01_P1.00mm_Horizontal" H 2900 2500 50  0001 C CNN
F 3 "~" H 2900 2500 50  0001 C CNN
	1    2900 2500
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0104
U 1 1 60C3766A
P 3000 2400
F 0 "#FLG0104" H 3000 2475 50  0001 C CNN
F 1 "PWR_FLAG" H 3000 2573 50  0000 C CNN
F 2 "" H 3000 2400 50  0001 C CNN
F 3 "~" H 3000 2400 50  0001 C CNN
	1    3000 2400
	1    0    0    -1  
$EndComp
Connection ~ 3000 2400
Wire Wire Line
	3000 2400 4250 2400
Text HLabel 5200 3400 2    50   Input ~ 0
12Vcc
$EndSCHEMATC
